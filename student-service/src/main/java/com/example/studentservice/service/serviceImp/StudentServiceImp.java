package com.example.studentservice.service.serviceImp;

import com.example.commonservice.model.dto.CourseDto;
import com.example.commonservice.model.response.NotFound;
import com.example.studentservice.model.dto.StudentDto;
import com.example.studentservice.model.entity.Student;
import com.example.studentservice.model.request.StudentRequest;
import com.example.studentservice.repository.StudentRepository;
import com.example.studentservice.service.StudentService;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentServiceImp implements StudentService {
    private final StudentRepository studentRepository;

    private final WebClient.Builder webClientCourse;

    public StudentServiceImp(StudentRepository studentRepository, WebClient.Builder webClientCourse) {
        this.studentRepository = studentRepository;
        this.webClientCourse = webClientCourse;
    }

    @Override
    public Student createStudent(StudentRequest studentRequest) {
        return studentRepository.save(studentRequest.toEntity());
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Mono<String> getAllCourses() {
        return webClientCourse.baseUrl("http://localhost:8082/api/v1")
                .build()
                .get()
                .uri("/course") // Set the specific URI for adding a resource
                .retrieve()
                .bodyToMono(String.class).log();
    }

    @Override
    public List<StudentDto> getAll() {
        List<Student> studentList = studentRepository.findAll();
        List<StudentDto> studentDto = new ArrayList<>();
        for(Student student: studentList ){
          CourseDto courseDto = webClientCourse
                   .baseUrl("http://localhost:8082/api/v1")
                    .build()
                    .get()
                    .uri("/course/{id}",student.getCourseId())
                    .retrieve()
                    .bodyToMono(CourseDto.class)
                    .block();
              studentDto.add(new StudentDto(student.getId(),student.getFirstName(),student.getLastName(),student.getEmail(),student.getBirthdate(),courseDto));
        }
        return studentDto;
    }

    @Override
    public StudentDto deleteStudentById(Long id) {
//        getStudentById(id);
       studentRepository.deleteById(id);
       return null;
    }

    @Override
    public StudentDto getStudentById(Long id) {

        return studentRepository.findById(id)
                .orElseThrow(
                () -> new NotFound(
                        "Not Found ",
                        "Course not found with id " + id
                )).toDto(new CourseDto());
    }

    @Override
    public StudentDto updateStudentById(StudentRequest studentRequest, Long id) {
//        getStudentById(id);
        Student student = studentRequest.toEntityById(id);
        return studentRepository.save(student).toDto(new CourseDto());
    }

}
