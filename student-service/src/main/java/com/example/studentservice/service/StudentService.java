package com.example.studentservice.service;

import com.example.studentservice.model.dto.StudentDto;
import com.example.studentservice.model.entity.Student;
import com.example.studentservice.model.request.StudentRequest;
import reactor.core.publisher.Mono;

import java.util.List;

public interface StudentService {

    Student createStudent(StudentRequest studentRequest);

    List<Student> getAllStudents();

    Mono<String> getAllCourses();

    List<StudentDto> getAll();

    StudentDto deleteStudentById(Long id);

    StudentDto getStudentById(Long id);

    StudentDto updateStudentById(StudentRequest studentRequest, Long id);
}
