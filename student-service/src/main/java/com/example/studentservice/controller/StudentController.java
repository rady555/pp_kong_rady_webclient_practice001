package com.example.studentservice.controller;

import com.example.commonservice.model.response.ResponseApi;
import com.example.studentservice.model.dto.StudentDto;
import com.example.studentservice.model.entity.Student;
import com.example.studentservice.model.request.StudentRequest;
import com.example.studentservice.service.StudentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/student-course")
    public ResponseEntity<List<StudentDto>> getAll(){
        return ResponseEntity.ok().body(studentService.getAll());
    }

    @GetMapping("/all/students")
    public ResponseEntity<List<Student>> getAllStudent(){
        return ResponseEntity.ok().body(studentService.getAllStudents());
    }

//    @GetMapping("/course")
//    public Mono<ResponseEntity<String>> getAllCourse(){
//        return studentService.getAllCourses()
//                .map(data -> ResponseEntity.ok().body(data))
//                .defaultIfEmpty(ResponseEntity.notFound().build());
//    }
    @PostMapping("/student")
    public ResponseEntity<?> createUser(@RequestBody StudentRequest studentRequest){
        return ResponseEntity.ok().body(studentService.createStudent(studentRequest));
    }

    //not work correct yet
//    @GetMapping("/student/{id}")
//    public ResponseEntity<ResponseApi<?>> getById(@PathVariable Long id){
//        ResponseApi<?> response = ResponseApi.<StudentDto>builder()
//                .message("Successfully got student by id "+ id)
//                .status("200")
//                .payload(studentService.getStudentById(id))
//                .build();
//        return ResponseEntity.ok().body(response);
//    }

    @DeleteMapping("/student/{id}")
    public ResponseEntity<ResponseApi<?>> delete(@PathVariable Long id){
        ResponseApi<?> response = ResponseApi.<StudentDto>builder()
                .message("Successfully deleted student id "+ id)
                .status("200")
                .payload(studentService.deleteStudentById(id))
                .build();
        return ResponseEntity.ok().body(response);
    }
    //not work correct yet
//    @PutMapping("/student")
//    public  ResponseEntity<ResponseApi<?>> update(@RequestBody StudentRequest studentRequest, @PathVariable Long id){
//        ResponseApi<?> response = ResponseApi.<StudentDto>builder()
//                .message("Successfully updated student id "+id)
//                .status("200")
//                .payload(studentService.updateStudentById(studentRequest,id))
//                .build();
//        return ResponseEntity.ok().body(response);
//    }
}
