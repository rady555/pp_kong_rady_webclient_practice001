package com.example.studentservice.model.request;


import com.example.studentservice.model.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.UUID;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentRequest {

    private String firstName;
    private String lastName;
    private String email;
    private LocalDateTime birthdate;
    private Long courseId;

    public Student toEntity(){
        return new Student(null, this.firstName,this.lastName,this.email,this.birthdate,this.courseId);
    }
    public Student toEntityById(Long id){
        return new Student(id,this.firstName,this.lastName,this.email,this.birthdate,this.courseId);
    }


}
