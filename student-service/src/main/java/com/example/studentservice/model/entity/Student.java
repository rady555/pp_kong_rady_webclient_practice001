package com.example.studentservice.model.entity;

import com.example.commonservice.model.dto.CourseDto;
import com.example.studentservice.model.dto.StudentDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    @Column(unique = true)
    private String email;
    @Temporal(value =  TemporalType.TIMESTAMP)
    private LocalDateTime birthdate;
    private Long courseId;

    public StudentDto toDto(CourseDto courseDto){
        return new StudentDto(this.id, this.firstName, this.lastName, this.email, this.birthdate, courseDto);
    }

//    public Student (UUID id, String firstName, String lastName, String email, Date birthdate){
//        this.id=id;
//        this.firstName=firstName;
//        this.lastName=lastName;
//        this.email=email;
//        this.birthdate=birthdate;
//
//
//    }
}
