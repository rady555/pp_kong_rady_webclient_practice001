package com.example.courseservice.service;

import com.example.commonservice.model.dto.CourseDto;
import com.example.courseservice.model.entity.Course;
import com.example.courseservice.model.request.CourseRequest;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CourseService {
    List<CourseDto> getAllCourses();


   CourseDto addCourse(CourseRequest courseRequest);

    CourseDto getCourseById(Long id);

    CourseDto deleteCourseById(Long id);

    CourseDto updateById(CourseRequest courseRequest, Long id);
}
