package com.example.courseservice.service.serviceImp;

import com.example.commonservice.model.dto.CourseDto;
import com.example.commonservice.model.response.NotFound;
import com.example.courseservice.model.entity.Course;
import com.example.courseservice.model.request.CourseRequest;
import com.example.courseservice.repository.CourseRepository;
import com.example.courseservice.service.CourseService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseServiceImp implements CourseService {
    private final CourseRepository courseRepository;

    public CourseServiceImp(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<CourseDto> getAllCourses() {
        return courseRepository.findAll().stream().map(Course::toDto).collect(Collectors.toList());
    }

    @Override
    public CourseDto addCourse(CourseRequest courseRequest) {
        var courseEntity = courseRequest.toEntity();
        return courseRepository.save(courseEntity).toDto();
    }

    @Override
    public CourseDto getCourseById(Long id) {
        return courseRepository.findById(id).orElseThrow(
                ()->  new NotFound(
                        "Not Found ",
                        "Course not found with id " + id
                )).toDto();
    }

    @Override
    public CourseDto deleteCourseById(Long id) {
        getCourseById(id);
        courseRepository.deleteById(id);
        return null;
    }

    @Override
    public CourseDto updateById(CourseRequest courseRequest, Long id) {
        getCourseById(id);
        Course course = courseRequest.toEntityById(id);
        return courseRepository.save(course).toDto();
    }
}
