package com.example.courseservice.controller;

import com.example.commonservice.model.dto.CourseDto;
import com.example.commonservice.model.response.ResponseApi;
import com.example.courseservice.model.entity.Course;
import com.example.courseservice.model.request.CourseRequest;
import com.example.courseservice.service.CourseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/course")

public class CourseController {

    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("")
    public ResponseEntity<List<CourseDto>> getAllCourse(){
        return ResponseEntity.ok().body(courseService.getAllCourses());
    }
    @GetMapping("/{id}")
    public ResponseEntity<CourseDto> getById(@PathVariable Long id){
        return ResponseEntity.ok().body(courseService.getCourseById(id));
    }

    @PostMapping("")
    public ResponseEntity<ResponseApi<?>> createCourse(@RequestBody CourseRequest courseRequest){
        ResponseApi<?> response = ResponseApi.<CourseDto>builder()
                .message("Successfully added category!")
                .status("200")
                .payload(courseService.addCourse(courseRequest))
                .build();
        return ResponseEntity.ok().body(response);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseApi<?>> deleteById(@PathVariable Long id){
        ResponseApi<?> response = ResponseApi.<CourseDto>builder()
                .message("Successfully deleted course id "+ id )
                .status("200")
                .payload(courseService.deleteCourseById(id))
                .build();
        return ResponseEntity.ok().body(response);
    }
    @PutMapping("/{id}")
    public ResponseEntity<ResponseApi<?>> update(@RequestBody CourseRequest courseRequest,@PathVariable Long id){
        ResponseApi<?> response = ResponseApi.<CourseDto>builder()
                .message("Successfully updated course id "+ id)
                .status("200")
                .payload(courseService.updateById(courseRequest,id))
                .build();
        return ResponseEntity.ok().body(response);
    }
}
