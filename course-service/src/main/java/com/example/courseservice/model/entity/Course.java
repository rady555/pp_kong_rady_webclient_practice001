package com.example.courseservice.model.entity;

import com.example.commonservice.model.dto.CourseDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "courses")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String courseName;
    private String courseCode;
    private String  courseDescription;
    private String instructor;

    public CourseDto toDto(){
        return new CourseDto(this.id,this.courseName,this.courseCode,this.courseDescription,this.instructor);
    }

    public Course(String courseName,String courseCode, String courseDescription, String instructor){
        this.courseName = courseName;
        this.courseCode = courseCode;
        this.courseDescription = courseDescription;
        this.instructor = instructor;
    }
}
