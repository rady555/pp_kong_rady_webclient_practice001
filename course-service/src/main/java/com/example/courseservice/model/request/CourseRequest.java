package com.example.courseservice.model.request;

import com.example.courseservice.model.entity.Course;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseRequest {

    private String courseName;
    private String courseCode;
    private String  courseDescription;
    private String instructor;

    public Course toEntity(){
        return new Course(this.courseName,this.courseCode,this.courseDescription,this.instructor);
    }
    public Course toEntityById(Long id){
        return new Course(id,this.courseName,this.courseCode,this.courseDescription,this.instructor);
    }
}
