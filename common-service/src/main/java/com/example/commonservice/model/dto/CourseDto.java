package com.example.commonservice.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CourseDto {
    private Long id;
    private String courseName;
    private String courseCode;
    private String  courseDescription;
    private String instructor;


}
