package com.example.commonservice.model.response;

import lombok.Builder;
import org.springframework.web.bind.annotation.ControllerAdvice;


public class NotFound extends RuntimeException{
    public String title;

    public NotFound(String title, String message) {
        super(message);
        this.title = title;
    }

}
