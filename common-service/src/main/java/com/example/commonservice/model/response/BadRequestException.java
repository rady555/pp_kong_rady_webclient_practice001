package com.example.commonservice.model.response;

import lombok.Builder;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
@Builder
public class BadRequestException {
}
