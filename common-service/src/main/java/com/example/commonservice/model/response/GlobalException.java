package com.example.commonservice.model.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.net.URI;

@ControllerAdvice
public class GlobalException {
    @ExceptionHandler(NotFound.class)
    public ProblemDetail notFound(
            NotFound notFoundException
    ) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(
                HttpStatus.NOT_FOUND, notFoundException.getMessage()
        );
        problemDetail.setTitle(notFoundException.title);
        problemDetail.setType(URI.create("localhost:8083/error/"));
        return problemDetail;
    }



}
